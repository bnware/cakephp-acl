<?php
/**
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;

if (!Configure::read('Acl.classname')) {
    Configure::write('Acl.classname', 'DbAcl');
}
if (!Configure::read('Acl.database')) {
    Configure::write('Acl.database', 'default');
}
if (!Configure::read('Acl.AcosTable')) {
    Configure::write('Acl.AcosTable', 'acos');
}
if (!Configure::read('Acl.ArosTable')) {
    Configure::write('Acl.ArosTable', 'aros');
}
if (!Configure::read('Acl.PermissionsTable')) {
    Configure::write('Acl.PermissionsTable', 'aros_acos');
}
